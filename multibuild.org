#+Title: Multibuild

* Usage

- In project root, invoke ~sbt nativeImage~
- Given required environment (graal etc..), this will generate an executable in ~target/native-image/~ folder.
